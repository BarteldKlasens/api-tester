package core.apitester;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class GitlabAPI {

    private static String PROJECT_BY_ID = "https://gitlab.com/api/v4/projects/{projectid}";
    private EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();
    private String accesToken = variables.getProperty("apikey");

    @Step("I look up a public project by id {0}")
    public void fetchProjectDetails(String projectid) {
        SerenityRest.given()
                .pathParam("projectid", projectid)
                .get(PROJECT_BY_ID);
    }

    @Step("I look up a private project by id {0}")
    public void fetchPrivateProjectDetails(String projectid) {
        SerenityRest.given()
                .auth().oauth2(accesToken)
                .pathParam("projectid", projectid)
                .get(PROJECT_BY_ID);
    }
}