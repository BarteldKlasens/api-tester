package core.apitester;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;

public class ProjectStepDefinitions {

    @Steps
    GitlabAPI gitlabAPI;

    @When("I look up a project by id {word} without credentials")
    public void lookUpAPublicProject(String projectid) {
        gitlabAPI.fetchProjectDetails(projectid);
    }

    @Given("I have a private Project with id {word}")
    public void lookUpAPrivateProject(String projectid) {
        gitlabAPI.fetchPrivateProjectDetails(projectid);
        restAssuredThat(response -> response.statusCode(200));
    }

    @Then("the resulting project information should contain {}")
    public void theResultingProjectShouldBe(String projectName) {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body(ProjectResponse.NAME, equalTo(projectName)));
    }

    @Then("I should get a 404")
    public void theResultingProjectShouldNotBeFound() {
        restAssuredThat(response -> response.statusCode(404));
    }
}