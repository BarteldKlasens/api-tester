Feature: Looking gitlab Projects

  Scenario Outline: Looking up public projects one gitlab
    When I look up a project by id <Project Id> without credentials
    Then the resulting project information should contain <Project Name>
    Examples:
      | Project Id | Project Name |
      | 29285562   | API Tester   |

  Scenario Outline: Looking up private projects one gitlab
    Given I have a private Project with id <Project Id>
    When I look up a project by id <Project Id> without credentials
    Then I should get a 404

    Examples:
      | Project Id |
      | 3674784   |
