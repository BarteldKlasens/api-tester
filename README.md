# API Tester

Api tester is a test suite to verify the gitlab api. Documentation for which can be found [here](https://docs.gitlab.com/ee/api/).

## Getting started
requirements:
Java
Maven

Checkout this project. In the top level directory run `mvn clean verify -Dapikey={access token for gitlab}`
An access token is needed to view the private parts of the api. instuctions on how to create one can be found [here](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html). The account used for this token must have access to the project listed as private in the tests.

## Adding tests
We make use of Serenity with Cucumber 6 and RestAssured. Documentation and examples can be found [here](https://github.com/serenity-bdd). Be sure to make new feature files for different parts of the API and/or human intent. example: search for a project, Manage users and setting up new Projects, should all been in differnt feature files.